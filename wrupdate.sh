#!/bin/sh

cd "$(dirname "$0")"
curl -s 'https://webring.otomir23.me/15/data' | jq -r ".prev.name" > layouts/partials/wrprev.html
curl -s 'https://webring.otomir23.me/15/data' | jq -r ".next.name" > layouts/partials/wrnext.html
cd -

