# [lumaeris.com](https://lumaeris.com/) 🌟

suddenly my new web site

UPD 2025: This file is pretty much outdated. Work on the new site will start from scratch probably soon, using a different SSG.

## To-do

~~- [ ] Placeholder for my future music on `/music`~~
- [ ] A new short domain for redirects
- [x] Redirects
- [x] Blog in `/blog`
- [x] Applying the ~~submodule~~ (redirect) for `/wsl` ([WSL-AltInstaller](https://github.com/Lumaeris/WSL-AltInstaller))
- [x] Create layout for main page and blog
- [ ] REcreate layout for main page and blog
- [ ] Possibly federated comments?
- [ ] Preview version on `preview.lumaeris.com`/`new.lumaeris.com`
- [ ] Check out some themes created for Hugo to take some inspiration
- [ ] Possibly use Bulma? idk i'm bored
- [ ] Fill out this list
